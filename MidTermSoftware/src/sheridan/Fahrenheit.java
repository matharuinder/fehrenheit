package sheridan;

public class Fahrenheit {
	
	public static void main(String[] args) {
		System.out.println(convertFromCelsius(-273));
	}

	public static int convertFromCelsius(int num) {
		
		return (int)(num*(9.0/5.0)+32);
		//
	}
	
}
