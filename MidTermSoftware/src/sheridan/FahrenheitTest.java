package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

public class FahrenheitTest {
//testing method
	@Test
	public void testConvertFromCelsiusA() {
		int f = Fahrenheit.convertFromCelsius(10);
		assertTrue("",f == 50);
	}
	
	@Test
	public void testConvertFromCelsiusB() {
		int f = Fahrenheit.convertFromCelsius(-273);
		assertTrue("",f == -459);
	}
	
	@Test
	public void testConvertFromCelsiusC() {
		int f = Fahrenheit.convertFromCelsius(0);
		assertTrue("",f == 32);
	}
	
	@Test
	public void testConvertFromCelsiusD() {
		int f = Fahrenheit.convertFromCelsius(10000);
		assertTrue("",f == 18032);
	}


}
